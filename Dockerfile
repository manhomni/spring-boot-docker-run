FROM niaquinto/gradle
MAINTAINER Palanivelrajan Balasubramanian <prajan@manh.com>

# In case someone loses the Dockerfile
RUN rm -rf /etc/Dockerfile
ADD Dockerfile /etc/Dockerfile

# Set your default behavior
ENTRYPOINT ["gradle"]
CMD ["clean", "war"]